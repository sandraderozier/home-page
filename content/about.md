+++
menu = "main"
title = "About"
type = "about"
weight = 2
+++

I work as a bioinformatics engineer at INRAE, on the Campus of Jouy-en-Josas, in MaIAGE: Applied Mathematics and Computer Science, from Genome to Environment.

### Background

- Since 2020/01 - Engineer in bioinformatics at [INRAE](https://www.inrae.fr/en) [MaIAGE](https://maiage.inrae.fr/en/node) [StatInfOmics](https://maiage.inrae.fr/en/statinfomics)
- 2010/10-2019/12 - Engineer in bioinformatics at [INRAE](https://www.inrae.fr/en) [MaIAGE](https://maiage.inrae.fr/en/node) [Migale](https://migale.inrae.fr/)
- 2008/09-2010/09 - Fixed-term contract engineer at INRA URGV
- 2008/03-2008/09 - Master 2 internship at [INRA URGI](https://urgi.versailles.inra.fr/) - supervisor: Delphine Steinbach

### Degrees

- 2008: Master 2 Structure, Proteome and Functional Genomics Option Software engineering - [Université Paris Diderot](https://u-paris.fr/en/)
- 2007: Master 1 Structure, Proteome and Functional Genomics - [Université Paris Diderot](https://u-paris.fr/en/)
- 2006: License Cell Biology and Physiology - [Université Evry Val d'Essonne](https://international.univ-evry.fr/welcome.html)
- 2005: DEUG Life and earth sciences - [Université Evry Val d'Essonne](https://international.univ-evry.fr/welcome.html)
- 2003: Bachelor - [Lycée La Mare Carrée](http://www.lamarecarree.fr/)
