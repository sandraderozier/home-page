+++
date = "2024-02-06T12:00:00-00:00"
title = "Networking"
+++

I am vice-president of the [French Bioinformatic Society (SFBI)](https://www.sfbi.fr/), co-leader of [CATI SysMics](https://sysmics.cati.inrae.fr/), a member of the [PEPI IBIS](https://pepi-ibis.inrae.fr/) board and coordinator of the Ile-de-France bioinformaticians network.

<!--more-->

#### French Bioinformatic Society - SFBI

The [French Bioinformatic Society (SFBI)](https://www.sfbi.fr/) is the learned society for bioinformatics. It aims to promote interdisciplinary research at the interface of biology, computer science, mathematics, statistics and physics, and to bring together the French-speaking bioinformatics community. It is an association (under the French law of 1901) independent of institutes and companies, representing the entire bioinformatics community, whatever the scientific field or laboratory (private or public). SFBI represents the entire bioinformatics community: researchers, students, teacher-researchers, engineers, laboratories and platforms, and covers all disciplinary fields (interface with molecular biology, computer science, maths and statistics, physics). Since 2021, SFBI has been a founding, active and associate member of the ["Collège des Sociétés Savantes Académiques de France"](https://societes-savantes.fr/) (CoSSAF) and, since 2023, a member of the [International Society for Computational Biology (ISCB)](https://www.iscb.org/).

I was elected to the **SFBI board** in 2022 and **vice-president** of the SFBI in 2023.

#### CATI SysMics

Since they were set up in 2008, the [CATIs ("Centres Automatisés de Traitement de l'Information" - "Automated centers of information processing")](https://pepi2g.wiki.inrae.fr/doku.php/communaute:catis:cati) have provided IT production and services to support a targeted scientific community. The [CATI SysMics](https://sysmics.cati.inrae.fr/) aims to develop a computing environment that incorporates the notion of (dynamic) processes in order to represent a living organism and to deploy statistical methods that exploit both heterogeneous -omic data (transcriptome, proteome, fluxome, metabolome, phenome, images, etc.) and processes.

I am **co-leader** of the CATI SysMics since 2021 and **leader** of the ["heterogeneous data visualisation interfaces" workpackage](https://sysmics.cati.inrae.fr/workpackage3).

#### PEPI IBIS

The aim of the [PEPI ("Partage d'expérience et de pratiques en informatique" - "Sharing experience and practices in IT")](https://pepi2g.wiki.inrae.fr/) is to exchange, share practices and questions, improve skills collectively, experiment with and pool tools and methods, discover new technologies, support and help each other... PEPIs are open to all INRAE and associated UMR staff with an interest in IT and a desire to participate by sharing and experimenting. The [PEPI IBIS ("Ingénierie Bio Informatique et Statistique" - "Bioinformatics and Statistical Engineering")](http://pepi-ibis.inra.fr/) is aimed at INRAE engineers and researchers in bioinformatics, computer science, statistics or biology who are responsible for analysing or developing tools for analysing "omics" data. The PEPI offers opportunities to discuss tools, methods and, more generally, practices from the bioinformatics and statistics disciplines, in order to meet the new challenges posed by these new approaches.

Since 2016, I am a **board member** of the PEPI IBIS.

#### INRAE Ile-de-France bioinformaticians network

Since 2012, I've been organising and leading the INRAE bioinformaticians' network at Jouy-en-Josas.
 
Since 2024, this network has been extended to the INRAE centers in Ile-de-France: Antony, Palaiseau, Saclay and Versailles.
