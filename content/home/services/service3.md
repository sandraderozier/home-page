+++
date = "2024-02-06T12:00:00-00:00"
title = "Development"
+++

I'm a development engineer in bioinformatics. In particular, I develop web interfaces as well as databases. I'm also working on making bioinformatics analysis workflows available.

<!--more-->

Through various projects and collaborations, I've been able to put my development skills to good use in proposing solutions tailored to the research problems of the scientific community.

**Main programming languages or tools used**   
![](../../../images/logos/LogoPython.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/FlaskLogo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/JavaLogo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/JavascriptLogo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/jQueryLogo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/d3Logo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/ShinyLogo.png)&nbsp;&nbsp;&nbsp;
![](../../../images/logos/PostgresqlLogo.png)&nbsp;&nbsp;&nbsp;&nbsp;
![](../../../images/logos/SQLiteLogo.png)&nbsp;&nbsp;&nbsp;&nbsp;
![](../../../images/logos/neo4jLogo.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![](../../../images/logos/GalaxyLogo.png)&nbsp;&nbsp;&nbsp;&nbsp;
![](../../../images/logos/SnakemakeLogo.png)&nbsp;
![](../../../images/logos/GitHubLogo.png)&nbsp;
![](../../../images/logos/gitlabLogo.png)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
![](../../../images/logos/jupyterLogo.png)&nbsp;&nbsp;&nbsp;
