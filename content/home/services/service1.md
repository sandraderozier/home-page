+++
date = "2024-02-06T12:00:00-00:00"
title = "Training"
+++

Since 2011, I have been actively involved in the ["Bioinformatics through practice" cycle](https://documents.migale.inrae.fr/trainings.html) organised by the [Migale platform](https://migale.inrae.fr/). From 2019 to 2021, I was co-leader of the Python module of the [DUBii](https://www.france-bioinformatique.fr/en/dubii/).

<!--more-->

### “Bioinformatics through practice” cycle

Since 2011, I have been actively involved in the ["Bioinformatics through practice" cycle](https://documents.migale.inrae.fr/trainings.html) organised by the [Migale platform](https://migale.inrae.fr/). This cycle covers a broad spectrum of bioinformatics subjects. All modules include both introduction on concepts and skills and practical activities. This cycle is open to all employees (public or private).

#### 2025
- [R Shiny application development](https://documents.migale.inrae.fr/posts/trainings/current/french/module26/) (module 26) - 14 March
- [Introduction to Python](https://documents.migale.inrae.fr/posts/trainings/current/french/module14/) (module 14) - 31 March/1st April
- [Advanced Python](https://documents.migale.inrae.fr/posts/trainings/current/french/module14bis/) (module 14bis) - 2-3 April

#### 2024
- [Introduction to Python](https://documents.migale.inrae.fr/posts/trainings/french/module14/) (module 14) - 26-27 March 
- [Advanced Python](https://documents.migale.inrae.fr/posts/trainings/french/module14bis/) (module 14bis) - 28-29 March 

#### 2023
- [R Shiny application development](https://migale.inrae.fr/sites/default/files/formation/2023/module26.pdf) (module 26) - 20 March 
- [Introduction to Python](https://migale.inrae.fr/sites/default/files/formation/2023/module14.pdf) (module 14) - 22-23 May 
- [Advanced Python](https://migale.inrae.fr/sites/default/files/formation/2023/module14bis.pdf) (module 14bis) - 24-25 May 

#### 2022
- [R Shiny application development](https://migale.inrae.fr/sites/default/files/formation/2022/module26.pdf) (module 26) - 18 March 
- [Introduction to Python](https://migale.inrae.fr/sites/default/files/formation/2022/module14.pdf) (module 14) - 13-14 June 
- [Advanced Python](https://migale.inrae.fr/sites/default/files/formation/2022/module14bis.pdf) (module 14bis) - 15-16 June 

#### 2021
- [R Shiny application development](https://migale.inrae.fr/sites/default/files/formation/2021/module26.pdf) (module 26) - 12 March 

#### 2019
- [Python](https://genome.jouy.inra.fr/tmp//metrics/pdf/2019/module14.pdf) (module 14) - 25-28 March 
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2019/module17.pdf) (module 17) - 13 May 
- [Bioinformatics processing and differential of RNA-seqxpression datan Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2019/module23.pdf) (module 23) - 14 June 
- [Perl](https://genome.jouy.inra.fr/tmp/metrics/pdf/2019/module11.pdf) (module 11) - 17-20 June 

#### 2018
- [Introduction to Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2018/module14.pdf) (module 14) - 29-30 March 
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2018/module17.pdf) (module 17) - 14 May 
- [Bioinformatics processing and differential of RNA-seqxpression datan Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2018/module23.pdf) (module 23) - 21 June 

#### 2017
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2017/module17.pdf) (module 17) - 13 March 
- [Introduction to Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2017/module14.pdf) (module 14) - 22-23 May 
- [Advanced Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2017/module14bis.pdf) (module 14bis) - 24 May 

#### 2016
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module17-15mars.pdf) (module 17) - 15 March 
- [Introduction to Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module14.pdf) (module 14) - 30-31 March 
- [Advanced Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module14bis.pdf) (module 14bis) - 1 April 
- [Introduction to Perl](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module11.pdf) (module 11) - 18-19 May 
- [Advanced Perl](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module11bis.pdf) (module 11bis) - 20 May 
- [Bioinformatics processing and differential of RNA-seqxpression datan Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module16.pdf) (module 16) - 23 May 
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2016/module17-30mai.pdf) (module 17) - 30 May 

#### 2015
- [Introduction to Perl](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module11.pdf) (module 11) - 31 March-1 April 
- [Advanced Perl](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module11bis.pdf) (module 11bis) - 2 April 
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module17-12mai.pdf) (module 17) - 12 May 
- [Introduction to Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module14.pdf) (module 14) - 26-27 May 
- [Advanced Python](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module14bis.pdf) (module 11bis) - 28 May 
- [Introduction to Galaxy](https://genome.jouy.inra.fr/tmp/metrics/pdf/2015/module17-2juin.pdf) (module 17) - 2 June 

In 2014 and 2015, I took part as a trainer in the course "Bioinformatics and biostatistical analysis of RNA-seq data" organised with INRAE continuing education. Between 2011 and 2014, I gave training courses every year on Perl and/or Python, as well as Galaxy.

### University Diploma in Integrative Bioinformatics (DUBii)

The [University of Paris](https://u-paris.fr/), in partnership with the [French National Bioinformatics infrastructure (IFB)](https://www.france-bioinformatique.fr/), is offering the [University Diploma in Integrative Bioinformatics (DUBii)](https://www.france-bioinformatique.fr/en/dubii/). This course is aimed primarily at biologists or doctors who wish to develop their skills or consider a career change and who have already acquired skills (short training, self-study, field experience) in computer science or bioinformatics / biostatistics (Unix, Python or R environment or other programming language).

The DUBii will provide theoretical and practical training, completed by a 20-day immersion period in one of the IFB's regional platforms, which will mobilise, within the framework of a tutored project, all the methods and tools learned during the courses to carry out a personal project in integrative bioinformatics. This project will combine data specific to each participant produced in his laboratory (BYOD principle: "Bring Your Own Data") or collected from public databases. 

I have been given the opportunity to co-lead the Python module from 2019 to 2021 with Patrick Fuchs and Pierre Poulain.
