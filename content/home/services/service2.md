+++
date = "2024-02-06T12:00:00-00:00"
title = "Supervision"
+++

As part of my work at INRAE and on various scientific projects, I supervise or co-supervise trainees (DUT, M1 and M2) and engineers, particularly on development tasks.

<!--more-->

**Sana GUEDOUAR** - Évaluation de Neo4J dans le cadre de la création du graphe de connaissances Omnicrobe - **M2 internship** - Université Paris Cité (2025-02-03 - 2025-07-31)

**Emilie-Jeanne MAYI** - Adaptation de l’application Genoscapist pour l’étude de l’hétérogénéité phénotypique et l’adaptation à l'hôte pendant la phase stationnaire chez les Firmicutes pathogènes sporulants - **M2 internship** - Université Paris Cité (2025-02-03 - 2025-07-31)

**Hajar Bouamout** - Evolution of the Omnicrobe application for the HoloOligo project - **Fixed-term contract engineer** (2024-12-02 - 2025-09-30)

**Zoé Le Roux** - Characterisation of Knowledge Graph through network tools: application to the Omnicrobe knowledge graph - **PhD student** (2024-11-01 - 2027-10-31)

**Emeline BRUYERE** - Construction, characterisation and visualisation of bacterial pangenomes at different evolutionary scales - **M2 internship** - Université Paris-Saclay (2024-03-04 - 2024-08-16)

**Elora VIGO** - Evolution of the Omnicrobe web interface to order data by quality - **M1 internship** - Université Paris-Saclay (2023-05-01 - 2023-06-30)

**Safiya ATIA** - Genoscapist, adding customising functionalities of graphic elements via the user interface - **M1 internship** - Université Paris-Saclay (2022-05-02 - 2022-07-29)

**Romane BEGHIN** - Genoscapist, adding on-the-fly data loading functionalities - **M1 internship** - Université Paris-Saclay (2022-05-02 - 2022-07-29)

**Nagi DEBBAH** - Development of a Snakemake workflow for pseudogene detection - **M1 internship** - Université Paris Diderot (2020-05-18 - 2020-08-14)

**Reda MEKDAD** - Omnicrobe API development - **Fixed-term contract engineer** (2019-08-01 - 2020-03-31)

**Sam AH-LONE** - Development of an application for aligning complete bacterial genomes - **M2 apprenticeship** - Université Rouen Normandie (2017-09-01 - 2019-08-31)

**Quentin CAVAILLE** - Development of a tool for cheese ecosystem analysis with metagenomic datasets - **Fixed-term contract engineer** (2017-10-02 - 2019-03-29)

**Sam AH-LONE** - Evaluation of published and available tools for aligning complete bacterial genomes (advantages/limitations) - **M1 internship** - Université Rouen Normandie (2017-03-01 - 2017-08-30)

**Thi-Phuong-Lien NGUYEN** - Integration of tools in Galaxy - **Fixed-term contract engineer** (2016-12-01 - 2018-06-30)

**Ghislain FIEVET** - Creation of an IMSV data warehouse - **Fixed-term contract engineer** (2015-10-01 - 2016-06-30)

**Stéphane PEILLET** - Integration of tools and workflows in Galaxy: quality control and improved reproducibility - **M2 internship** - Ecole d'ingénieurs Paris-Sud Ivry ESME (2014-04-01 - 2014-09-30)

**Yann GUARRACINO** - Implementation of NGS workflows in a Galaxy environment - **M1 internship** - Université Paris Diderot (2013-03-01 - 2013-05-31)

**Najla BEN HASSINE** - Study of the influence of the intestinal microbiota on the hypothalamic-pituitary-adrenal axis in the case of acute stress in rats using proteomic, transcriptomic and bioinformatics approaches - ****M2 internship**** - Université Evry Val d'Essonne (2012-01-01 - 2012-07-31)

**Guillaume REBOUL** - Implementation of a Genome Browser for rainbow trout - **DUT internship** - IUT Aurillac (2012-04-01 - 2012-06-30)
