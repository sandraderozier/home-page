+++
type = "subheader"
title = ""
+++

I work as a bioinformatics engineer at [INRAE](https://www.inrae.fr/en), on the Campus of Jouy-en-Josas, in [MaIAGE: Applied Mathematics and Computer Science, from Genome to Environment](https://maiage.inrae.fr/en/).
