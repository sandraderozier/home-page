+++
date = "2024-02-06T12:00:00-00:00"
title = "Supervision"
+++

I don't think they tried to market it to the billionaire, spelunking, base-jumping crowd. i did the same thing to gandhi, he didn't eat for three weeks. i once heard a wise man say there are no perfect men.

<!--more-->

## Engineers 

MEKDAD Reda : du 2019-08-01 au 2020-03-31
Ingénieur en développement

CAVAILLE Quentin : du 2017-10-02 au 2019-03-29
CDD Développement web et Base de données

NGUYEN Thi-Phuong-Lien : du 2016-12-01 au 2018-06-30
Intégration Galaxy

FIEVET Ghislain : du 2015-10-01 au 2016-06-30
Création d'un entrepôt de données dans le cadre de l'IMSV

### Interns

Emeline BRUYERE - Construction, characterisation and visualisation of bacterial pangenomes at different evolutionary scales - Master 2 - Université Paris-Saclay (2024-03-04 - 2024-08-16)

Elora VIGO - Evolution of the Omnicrobe web interface to order data by quality - Master 1 - Université Paris-Saclay (2023-05-01 - 2023-06-30)

Safiya ATIA - Genoscapist, adding customising functionalities of graphic elements via the user interface - Master 1 - Université Paris-Saclay (2022-05-02 - 2022-07-29)

Romane BEGHIN - Genoscapist, adding on-the-fly data loading functionalities - Master 1 - Université Paris-Saclay (2022-05-02 - 2022-07-29)



Debbah Nagi : Développement d'un workflow Snakemake pour la détection de pseudogènes - Master 1 - Université de Paris Diderot : du 2020-05-18 au 2020-08-14

AH-LONE Sam : Mise en place d'une application permettant l'alignement de génomes complets bactériens - Master Biosciences, Bio-Informatique 2ème année en apprentissage - Université de ROUEN Normandie : du 2017-09-01 au 2019-08-31

AH-LONE Sam : Evaluation des outils d'alignement de génomes complets bactériens publiés et disponibles (avantages/limites) - Master Biosciences, Bio-Informatique 1ère année - Université de ROUEN Normandie : du 2017-03-01 au 2017-08-30

PEILLET Stéphane : Intégration d'outils et de workflows dans Galaxy : contrôle qualité et amélioration de la reproductibilité - Master 2 CI - ESME SUDRIA : du 2014-04-01 au 2014-09-30

GUARRACINO Yann : Implémentation de workflows ngs dans un environnement galaxy - Master 1 : Biologie - Informatique - Université Paris Diderot - Paris7 : du 2013-03-01 au 2013-05-31

BEN HASSINE Najla : Etude de l'influence du microbiote intestinal sur l'axe hypothalamo-hypophyso-surrénalien dans le cas du stress aigu chez le rat par des approches : protéomique, transcriptomique et bioinformatique - Master 2 Génomique et Post Génomique Spécialité Génie Biologique et Informatique - Université d'Evry Val d'Essonne : du 2012-01-01 au 2012-07-31

REBOUL Guillaume : Implémentation et mise en place d'un Genome Browser pour la truite arc-en-ciel - DUT Génie biologique, option Bio-informatique - IUT Aurillac : du 2012-04-01 au 2012-06-30

