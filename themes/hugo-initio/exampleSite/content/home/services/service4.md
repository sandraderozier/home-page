+++
date = "2024-02-06T12:00:00-00:00"
title = "Networking"
+++

I am vice-president of the French Bioinformatic Society (SFBI), co-leader of CATI SysMics, a member of the PEPI IBIS board and coordinator of the Ile-de-France bioinformaticians network.

<!--more-->

#### French Bioinformatic Society - SFBI

The French Bioinformatic Society (SFBI) is the **learned society for bioinformatics**. It aims to promote interdisciplinary research at the interface of biology, computer science, mathematics, statistics and physics, and to bring together the French-speaking bioinformatics community.

I was elected to the **SFBI board** in 2022 and **vice-president** of the SFBI in 2023.

For more information: <a href='https://www.sfbi.fr/'>https://www.sfbi.fr/</a>

#### CATI SysMics

For more information: <a href='https://sysmics.cati.inrae.fr/'>https://sysmics.cati.inrae.fr/</a>

#### PEPI IBIS

For more information: <a href='http://pepi-ibis.inra.fr/'>http://pepi-ibis.inra.fr/</a>

#### Bioinfo IDF

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
