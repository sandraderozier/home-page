+++
date = "2024-02-06T12:00:00-00:00"
title = "Training"
+++

Python, R Shiny, Perl, Galaxy, ... I don't think they tried to market it to the billionaire, spelunking, base-jumping crowd. i did the same thing to gandhi, he didn't eat for three weeks. i once heard a wise man say there are no perfect men.

<!--more-->

### “Bioinformatics through practice” cycle

Since 2011, I have been actively involved in the "Bioinformatics through practice" series organised by the Migale platform. This cycle covers a broad spectrum of bioinformatics subjects. All modules include both introduction on concepts and skills and practical activities. The training location is INRAE, Jouy-en-Josas campus, MaIAGE unit meeting room (Bat. 233). You will find here the different ways to come.

This cycle is open to all employees (public or private).

For more information: <a href='https://documents.migale.inrae.fr/trainings.html'>https://documents.migale.inrae.fr/trainings.html</a>.

### University Diploma in Integrative Bioinformatics (DUBii)

The University of Paris, in partnership with the Institut Français de Bioinformatique (IFB), is offering the third edition of the University Diploma in Integrative Bioinformatics (DUBii). This course is aimed primarily at biologists or doctors who wish to develop their skills or consider a professional reconversion and who have already acquired skills (short training, self-study, field experience) in computer science or bioinformatics / biostatistics (Unix, Python or R environment or other programming language). The prerequisites are described on the "DU" portal of the University of Paris, which presents the DUBii and the complementary DU "Création, Analyse et Valorisation de Données Omiques" (DUO).

The DUBii will provide theoretical and practical training, completed by a 20-day immersion period on one of the IFB's regional platforms, which will mobilise, within the framework of a tutored project, all the methods and tools learned during the courses to carry out a personal project in integrative bioinformatics. This project will combine data specific to each participant produced in his laboratory (BYOD principle: "Bring Your Own Data") or collected from public databases. 

For more information: <a href=''>https://www.france-bioinformatique.fr/en/dubii/</a>.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
