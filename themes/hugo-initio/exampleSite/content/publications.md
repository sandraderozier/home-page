+++
menu = "main"
title = "Publications"
type = "publication"
weight = 3
+++

Yasmine Dergham, Dominique Le Coq, Pierre Nicolas, Elena Bidnenko, **Sandra Dérozier**, Maxime Deforet, Eugénie Huillet, Pilar Sanchez-Vizuete, Julien Deschamps, Kassem Hamze & Romain Briandet. Direct comparison of spatial transcriptional heterogeneity across diverse Bacillus subtilis biofilm communities. *Nature Communications* 14, 7546 (2023). <a href='https://doi.org/10.1038/s41467-023-43386-w'>https://doi.org/10.1038/s41467-023-43386-w</a>

Etienne Dervyn, Anne-Gaëlle Planson, Kosei Tanaka, Victor Chubukov, Cyprien Guérin, **Sandra Derozier**, François Lecointe, Uwe Sauer, Ken-Ichi Yoshida, Pierre Nicolas, Philippe Noirot, Matthieu Jules. Greedy reduction of Bacillus subtilis genome yields emergent phenotypes of high resistance to a DNA damaging agent and low evolvability. *Nucleic Acids Research*, Volume 51, Issue 6, 11 April 2023, Pages 2974–2992 (2023). <a href='https://doi.org/10.1093/nar/gkad145'>https://doi.org/10.1093/nar/gkad145</a>

Vladimir Bidnenko, Pierre Nicolas, Cyprien Guérin, **Sandra Dérozier**, Arnaud Chastanet, Julien Dairou, Yulia Redko-Hamel, Matthieu Jules, Elena Bidnenko. Termination factor Rho mediates transcriptional reprogramming of Bacillus subtilis stationary phase. *PLOS Genetics* 19(2): e1010618 (2023). <a href='https://doi.org/10.1371/journal.pgen.1010618'>https://doi.org/10.1371/journal.pgen.1010618</a>

**Sandra Dérozier**, Robert Bossy, Louise Deléger, Mouhamadou Ba, Estelle Chaix, Olivier Harlé, Valentin Loux, Hélène Falentin, Claire Nédellec. Omnicrobe, an open-access database of microbial habitats and phenotypes using a comprehensive text mining and data fusion approach. *PLOS ONE* 18(1): e0272473 (2023). <a href="https://doi.org/10.1371/journal.pone.0272473">https://doi.org/10.1371/journal.pone.0272473</a>

Caroline Isabel Kothe, Alexander Bolotin, Bochra-Farah Kraïem, Bedis Dridi, Anne-Laure Abraham, Nacer Mohellibi, **Sandra Dérozier**, Valentin Loux, Pierre Renault. Unraveling the world of halophilic and halotolerant bacteria in cheese by combining cultural, genomic and metagenomic approaches. *International Journal of Food Microbiology*, Volume 358 (2021). <a href='https://doi.org/10.1016/j.ijfoodmicro.2021.109312'>https://doi.org/10.1016/j.ijfoodmicro.2021.109312</a>

**Sandra Dérozier**, Pierre Nicolas, Ulrike Mäder, Cyprien Guérin. Genoscapist: online exploration of quantitative profiles along genomes via interactively customized graphical representations. *Bioinformatics* (2021). <a href='https://doi.org/10.1093/bioinformatics/btab079'>https://doi.org/10.1093/bioinformatics/btab079</a>

Ulrike Mäder, Pierre Nicolas, Maren Depke, Jan Pané-Farré, Michel Debarbouille, Magdalena M. van der Kooi-Pol, Cyprien Guérin, **Sandra Dérozier**, Aurelia Hiron, Hanne Jarmer, Aurélie Leduc, Stephan Michalik, Ewoud Reilman, Marc Schaffer, Frank Schmidt, Philippe Bessières, Philippe Noirot, Michael Hecker, Tarek Msadek, Uwe Völker, Jan Maarten van Dijl. Staphylococcus aureus Transcriptome Architecture: From Laboratory to Infection-Mimicking Conditions. *PLOS Genetics* 12(4): e1005962 (2016). <a href='https://doi.org/10.1371/journal.pgen.1005962'>https://doi.org/10.1371/journal.pgen.1005962</a>

Noémie Pascault, Valentin Loux, **Sandra Derozier**, Véronique Martin, Didier Debroas, Selma Maloufi, Jean-François Humbert, Julie Leloup. Technical challenges in metatranscriptomic studies applied to the bacterial communities of freshwater ecosystems. *Genetica* 143, 157–167 (2015). <a href="https://doi.org/10.1007/s10709-014-9783-4">https://doi.org/10.1007/s10709-014-9783-4</a>

François Roudier, Ikhlak Ahmed, Caroline Bérard, Alexis Sarazin, Tristan Mary‐Huard, Sandra Cortijo, Daniel Bouyer, Erwann Caillieux, Evelyne Duvernois‐Berthet, Liza Al‐Shikhley, Laurène Giraut, Barbara Després, Stéphanie Drevensek, Frédy Barneche, **Sandra Dérozier**, Véronique Brunaud, Sébastien Aubourg, Arp Schnittger, Chris Bowler, Marie‐Laure Martin‐Magniette, Stéphane Robin, Michel Caboche, and Vincent Colot. Integrative epigenomic mapping defines four main chromatin states in Arabidopsis. *The EMBO Journal* 30: 1928 - 1938 (2011). <a href='https://doi.org/10.1038/emboj.2011.103'>https://doi.org/10.1038/emboj.2011.103</a>

**Sandra Dérozier**, Franck Samson, Jean-Philippe Tamby, Cécile Guichard, Véronique Brunaud, Philippe Grevet, Séverine Gagnot, Philippe Label, Jean-Charles Leplé, Alain Lecharny, Sébastien Aubourg. Exploration of plant genomes in the FLAGdb++ environment. *Plant Methods* 2011 Mar 29;7(1):8 (2011). <a href='https://doi.org/10.1186/1746-4811-7-8'>https://doi.org/10.1186/1746-4811-7-8</a>

